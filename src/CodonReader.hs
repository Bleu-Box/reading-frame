--  This module is for translating DNA codons to amino acids.

module CodonReader ( Nucleotide(..)
                   , Codon(..)
                   , Amino(..)
                   , aminos ) where

import Data.List.Split (chunksOf)

data Nucleotide = A | C | G | T
  deriving (Eq, Read, Show)
      
data Codon = Codon Nucleotide Nucleotide Nucleotide
  deriving (Eq, Show)

data Amino = Phenylalanine
             | Leucine
             | Isoleucine
             | Methionine
             | Valine
             | Serine
             | Proline
             | Threonine
             | Alanine
             | Tyrosine
             | Histidine
             | Glutamine
             | Asparagine
             | Lysine
             | Aspartic_Acid
             | Glutamic_Acid
             | Cysteine
             | Tryptophan
             | Arginine
             | Glycine
             | STOP
  deriving (Eq, Show)
             
aminos :: String -> Maybe [Amino]
aminos codons = translate codons'
  where codons' = chunksOf' 3 codons
        translate = fmap (map amino) . mapM parseCodon

parseCodon :: String -> Maybe Codon
parseCodon codon =
  case mapM parseNucleotide codon of
    Just [a, b, c] -> Just $ Codon a b c
    _ -> Nothing

parseNucleotide :: Char -> Maybe Nucleotide
parseNucleotide n
  | n `elem` "ACGT" = Just $ read [n]
  | otherwise = Nothing

-- This variant of `chunksOf` drops all the sublists that aren't
-- the specific chunk size. This is important because codon
-- reading will fail the whole amino chain if one codon isn't
-- long enough (i.e. if offsetting the reading frame results in stubby un-codons.)
chunksOf' :: Int -> [a] -> [[a]]
chunksOf' n = filter ((== n) . length) . chunksOf n

-- | Converts a codon to an amino acid.
amino :: Codon -> Amino
amino (Codon T T T) = Phenylalanine
amino (Codon T T C) = Phenylalanine
amino (Codon T T A) = Leucine
amino (Codon T T G) = Leucine
amino (Codon C T _) = Leucine
amino (Codon A T T) = Isoleucine
amino (Codon A T C) = Isoleucine
amino (Codon A T A) = Isoleucine
amino (Codon A T G) = Methionine
amino (Codon G T _) = Valine
amino (Codon T C _) = Serine
amino (Codon C C _) = Proline
amino (Codon A C _) = Threonine
amino (Codon G C _) = Alanine
amino (Codon T A T) = Tyrosine
amino (Codon T A C) = Tyrosine
amino (Codon T A A) = STOP
amino (Codon T A G) = STOP
amino (Codon C A T) = Histidine
amino (Codon C A C) = Histidine
amino (Codon C A A) = Glutamine
amino (Codon C A G) = Glutamine
amino (Codon A A T) = Asparagine
amino (Codon A A C) = Asparagine
amino (Codon A A A) = Lysine
amino (Codon A A G) = Lysine
amino (Codon G A T) = Aspartic_Acid
amino (Codon G A C) = Aspartic_Acid
amino (Codon G A A) = Glutamic_Acid
amino (Codon G A G) = Glutamic_Acid
amino (Codon T G T) = Cysteine
amino (Codon T G C) = Cysteine
amino (Codon T G A) = STOP
amino (Codon T G G) = Tryptophan
amino (Codon C G _) = Arginine
amino (Codon A G T) = Serine
amino (Codon A G C) = Serine
amino (Codon A G A) = Arginine
amino (Codon A G G) = Arginine
amino (Codon G G _) = Glycine
