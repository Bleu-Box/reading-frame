module Lib ( frame
           , longestFrame
           , readingFrames ) where

import CodonReader
import Data.List (elemIndex, maximumBy)
import Data.Maybe (fromJust, mapMaybe)
import Data.Ord (comparing)

type ReadingFrame = [Amino]

-- | Returns the start index of the longest reading frame.
-- -1 Means that none of the reading frames work.
longestFrame :: String -> Int
longestFrame codons =
  case bestFrame of
    [] -> -1
    otherwise -> fromJust $ elemIndex bestFrame frames 
  where frames = readingFrames codons
        bestFrame = longestSublist frames

longestSublist :: [[a]] -> [a]
longestSublist = maximumBy (comparing length)

-- | Returns a nested list of aminos representing each reading
-- frame of the DNA sequence.
readingFrames :: String -> [ReadingFrame]
readingFrames str = frame <$> aminoList
  where aminoList = mapMaybe aminos $ take 3 (iterate tail str)

-- | Trims a list of aminos into a reading frame by
-- slicing the list from the start to stop aminos.
frame :: [Amino] -> ReadingFrame
frame = takeWhile (/= STOP) . dropWhile (/= Methionine)
