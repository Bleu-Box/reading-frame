import CodonReader (Amino(..))
import Data.List (sort)
import Lib (frame, longestFrame, readingFrames)
import Test.Hspec

main :: IO ()
main = hspec $ do
  describe "Lib.frame" $ do
    it "should return the first occuring sublist from Methionine to STOP" $ do
      frame aminos `shouldBe` [Methionine, Valine, Serine]
  describe "Lib.readingFrames" $ do
    it "should return 3 shifts of a codon list translated to aminos" $ do
      readingFrames codons `shouldBe` [ [Methionine, Leucine]
                                      , [Methionine, Serine]
                                      , [Methionine, Serine, Glycine] ]
  describe "Lib.longestFrame" $ do
    it "should return the best reading index of a given codon sequence" $ do
      longestFrame codons `shouldBe` 2
    it "should return -1 if no reading frame can be found" $ do
      longestFrame "TTTAAAGGG" `shouldBe` -1
  where codons = "TTTATGTTGTAAATATGTCCGGGTGATGAGCTAA"
        aminos = [ Phenylalanine, Leucine, Methionine, Valine, Serine, STOP
                 , Leucine, Glycine, Methionine, Histidine, STOP, Glutamine ]
